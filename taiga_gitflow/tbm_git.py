"""Modulo para el soporte de git."""
from typing import List, NamedTuple, Callable
import subprocess
import os.path

from taiga_gitflow.tbm_util import (
    OK_BLUE, OK_GREEN, WARNING, INFO, FAIL,
    SECURE_COMMAND, INSECURE_COMMAND, RESET_COLOR,
    exit_with_error_msg)

public_branches = [
    'master',
    'develop',
    'release'
]

counterpart_branch_proxy = {
    'feature': 'develop',
    'hotfix': 'master',
    'bugfix': 'release',
    'release': 'develop'
}

SUCCESS_ERROR_LEVEL = 0


class Version(NamedTuple):
    """Clase de datos para almacenar la versión de un proyecto."""
    major: int
    release: int
    minor: int

    def __str__(self):
        return self.to_string

    def __eq__(self, other):
        return self.to_string == other.to_string

    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def to_string(self) -> str:
        """Devuelve la versión en formato texto básico."""
        return '{}.{}.{}'.format(*self)

    @property
    def release_tag(self) -> str:
        """Devuelve el tag con formato release candidate."""
        return 'v' + self.to_string + '-rc'

    @property
    def production_tag(self) -> str:
        """Devuelve el tag con formato de producción."""
        return 'v' + self.to_string


def launch_shell_command(command: str,
                         quiet=False,
                         return_list=False,
                         result_as_bool=False,
                         verbose: bool = False) -> [str, None,
                                                    bool, List[str]]:
    """Lanza un comando a consola y devuelve resultado."""
    with subprocess.Popen(command, shell=True, stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT) as process:

        command_error_level = process.wait()

        if result_as_bool:
            return command_error_level == SUCCESS_ERROR_LEVEL

        if command_error_level != SUCCESS_ERROR_LEVEL:
            if quiet:
                return
            else:
                _print_process_stdout(process.stdout)
                raise RuntimeError(
                    'se detiene el proceso por un error, ' +
                    'ver mensajes adicionales')

        if return_list:
            result = [_get_element_from_line(line) for line in
                      process.stdout.readlines()]
        else:
            result = process.stdout.readline()
            if result is not None:
                result = _binary_to_str(result)
        if verbose:
            _print_process_stdout(process.stdout)
            _print_process_stdout(process.stderr)

        return result


def try_sequential_git_commands(expected_branch_type: str,
                                git_command_list: List[str],
                                success_msg: str = None,
                                error_msg: str = None,
                                verbose: bool = False) -> None:
    """Lanza una serie de comandos git si no hay cambios pendientes."""
    generic_types = ['public', 'private']
    if expected_branch_type in generic_types:
        current_branch_type = 'public' if is_public_branch() else 'private'
    else:
        current_branch_type = get_current_branch_type()

    if expected_branch_type != current_branch_type:
        exit_with_error_msg("""
        ERROR: La rama actual no es una rama del tipo esperado %s.
        """ % expected_branch_type, severity=FAIL)
    _try_sequential_shell_commands(git_command_list, success_msg,
                                   error_msg,
                                   git_branch_may_be_clean=True,
                                   verbose=verbose)


def get_current_branch_full_name() -> str:
    """Devuelve la rama actual"""
    command_get_branch = 'git branch | grep \* | cut -c 3-'
    return launch_shell_command(command_get_branch)


def get_current_branch_type() -> str:
    """Devuelve el tipo de la rama actual."""
    branch = get_current_branch_full_name()
    return get_branch_type(branch)


def get_current_branch_id() -> str:
    """Devuelve el id de la rama."""
    current_branch = get_current_branch_full_name()
    if '/' in current_branch:
        branch_id = current_branch.split('/')[1]
        return branch_id
    else:
        exit_with_error_msg("""
            ERROR: La rama actual no tiene id.
            """, severity=FAIL)


def get_branch_type(branch: str) -> str:
    """Devuelve el tipo de la rama."""
    return branch.split('/')[0]


def get_counterpart_branch(branch: str) -> str:
    """Devuelve el nombre de la rama contraparte."""
    branch_type = get_branch_type(branch)

    try:
        counterpart_branch_type = counterpart_branch_proxy[branch_type]
    except KeyError:
        counterpart_branch_type = None

    counterpart_name = None
    if counterpart_branch_type == 'release':
        try:
            counterpart_name = get_local_release_branches()[0]
        except IndexError:
            exit_with_error_msg("""
            {}ERROR: No existe ninguna rama RELEASE sobre la que trabajar. 
            Revisa antes las ramas existentes.{}
            """, FAIL)
    else:
        counterpart_name = counterpart_branch_type

    return counterpart_name


def is_develop_branch() -> bool:
    """Comprueba si la rama es 'develop'"""
    return get_current_branch_type() == 'develop'


def is_feature_branch() -> bool:
    """Comprueba si la rama es de tipo feature."""
    return get_current_branch_type() == 'feature'


def is_public_branch() -> bool:
    """Comprueba si la rama es publica"""
    return get_current_branch_type() in public_branches


def have_untracked_files() -> bool:
    """Devuelve True si la rama tiene archivos pendientes de seguimiento."""
    untracked_files = launch_shell_command(
        'git ls-files --other --exclude-standard --directory '
        '--no-empty-directory', return_list=True)
    return True if untracked_files else False


def have_unstaged_files() -> bool:
    """Devuelve True si la rama tiene archivos con cambios no añadidos al
    siguiente commit."""
    return not launch_shell_command(
        'git diff --exit-code > /dev/null',
        result_as_bool=True)


def have_changes_ready_to_commit() -> bool:
    """Devuelve True si la rama tiene cambios añadidos para el siguiente
    commit."""
    return not launch_shell_command(
        'git diff --cached --exit-code > /dev/null',
        result_as_bool=True)


def all_changes_are_ready_to_commit() -> bool:
    """Devuelve True si la rama tiene todos los cambios añadidos para el
    siguiente commit."""
    return True if not have_untracked_files() and not have_unstaged_files() \
        else False


def have_modified_files() -> bool:
    """Devuelve True si la rama tiene archivos modificados."""
    return not launch_shell_command(
        'git diff-index --quiet HEAD --',
        result_as_bool=True)


def branch_is_dirty() -> bool:
    """Devuelve True si la rama tiene cambios, está sucia."""
    return have_modified_files() or have_untracked_files()


def exit_if_pending_changes() -> None:
    """Comprueba si hay cambios en la rama actual y sale en caso afirmativo."""

    if branch_is_dirty():

        current_branch = get_current_branch_full_name()

        if is_public_branch():
            error_msg = """
                ERROR: cambios pendientes en la rama publica {0}.
                
                NOTA: puede ser debido a diferencias con .gitignore, si es el
                caso copiar .gitignore de la rama privada a la publica, 
                en caso contrario aplicar el procedimiento siguiente.
                
                Guardalos con los comandos:
                    {1}git add -A{2}
                    {1}git stash{2}
                E inicia una tarea nueva y recuperalos con los comandos:
                    {1}tbm_start_team_task{2}
                    {3}git stash pop{2} 
                """.format(current_branch, INSECURE_COMMAND, RESET_COLOR,
                           SECURE_COMMAND)
            severity = FAIL
        else:
            error_msg = """
                WARNING: Tienes cambios pendientes en {}.
                Es necesario hacer un commit antes, puedes usar: "ctrl+k".
                """.format(current_branch)
            severity = WARNING

        exit_with_error_msg(error_msg, severity)


def branch_exists(branch: str) -> bool:
    """Devuelve True si la rama existe."""
    return launch_shell_command('git br | grep "{}"'.format(branch),
                                result_as_bool=True)


def update_branch_and_counterpart_and_delete_orphan_branches(
        start_branch: str):
    """Actualiza la rama y la contraparte y borra las locales huérfanas."""
    start_branch_type = get_current_branch_type()

    counterpart_branch = get_counterpart_branch(start_branch)
    if counterpart_branch is None:
        error_msg = """
        {}ERROR: La rama actual es de tipo {}.{}
        """.format(FAIL, start_branch_type, RESET_COLOR)
        exit_with_error_msg(error_msg)

    checkout_counterpart_command = 'git co {}'.format(counterpart_branch)
    update_command = 'git pull --prune'

    try_sequential_git_commands(start_branch_type, [
        checkout_counterpart_command, update_command
    ])

    delete_private_branches_if_are_orphan()
    delete_local_release_branches_if_are_orphan(counterpart_branch)

    if branch_exists(start_branch):
        expected_current_branch_type = get_branch_type(counterpart_branch)
        checkout_branch_of_origin_command = 'git co {}'.format(
            start_branch)
        if start_branch_type == 'release':
            update_command = 'git pull'
        else:
            update_command = 'git pull --rebase'
        try_sequential_git_commands(expected_current_branch_type,
                                    [checkout_branch_of_origin_command,
                                     update_command])
    else:
        error_msg = """
        {}WARNING: A continuación Se cancelará la ceremonia de inicio de 
        jornada porque la rama actual '{}' ha sido eliminada.{} 
        """.format(WARNING, start_branch, RESET_COLOR)
        exit_with_error_msg(error_msg)


def get_local_release_branches():
    """Devuelve una lista con las ramas locales."""
    local_release_branches = launch_shell_command('git branch | grep '
                                                  'release/', quiet=True,
                                                  return_list=True)

    return local_release_branches if local_release_branches is not None else []


def delete_orphan_branch(branch: str) -> None:
    """Borra la rama y muestra mensaje indicando que el motivo es que
    está huérfana."""
    launch_shell_command(
        'git branch -D {}'.format(branch))
    print("""
        {0}La rama {1} estaba huérfana. Se ha eliminado.{2}
        """.format(INFO, branch, RESET_COLOR))


def delete_local_release_branches_if_are_orphan(current_branch: str):
    """Si existe una roma release local huérfana, la elimina."""

    local_release_branches = get_local_release_branches()

    exit_with_error = False

    for branch in local_release_branches:
        if branch_is_orphan(branch):
            if branch == current_branch:
                launch_shell_command('git co develop')
                exit_with_error = True
            delete_orphan_branch(branch)

    if exit_with_error:
        error_msg = """
        {}WARNING: Se ha borrado la rama release implicada en la ceremonia{}.
        """.format(WARNING, RESET_COLOR)
        exit_with_error_msg(error_msg)


def delete_private_branches_if_are_orphan():
    """Elimina las ramas privadas que están huérfanas."""
    get_local_branches_command = 'git br | cut -c3- | cut -f1 -d " "'
    local_branches = launch_shell_command(get_local_branches_command,
                                          return_list=True)
    for branch in local_branches:
        if branch in public_branches:
            continue
        elif branch_is_orphan(branch):
            delete_orphan_branch(branch)


def get_contain_branch_history_branches(branch: str) -> List[str]:
    """Devuelve la lista de las ramas que tienen las historia completa de la
     rama."""
    get_has_branch_history_branches_command = \
        'git br --contains {} | cut -c3- | cut -f1 -d " "'.format(branch)

    has_branch_history_branches = launch_shell_command(
        get_has_branch_history_branches_command, return_list=True)

    return has_branch_history_branches


def counterpart_has_full_branch_history(branch: str) -> bool:
    """Devuelve True si la contraparte tiene la historia completa de la
    rama."""

    has_branch_history_branches = get_contain_branch_history_branches(
        branch)
    counterpart_branch = get_counterpart_branch(branch)

    return True if counterpart_branch in has_branch_history_branches else False


def branch_has_full_counterpart_history(branch: str) -> bool:
    """Devuelve True si la rama tiene la historia completa de la
    contraparte."""

    counterpart_branch = get_counterpart_branch(branch)
    has_counterpart_history_branches = get_contain_branch_history_branches(
        counterpart_branch)

    return True if branch in has_counterpart_history_branches else False


def branch_is_orphan(branch: str) -> bool:
    """Devuelve True si la rama está huérfana."""
    branch_was_gone_command = "git branch -vv | grep '{}' | grep ': " \
                              "gone]'".format(branch)

    not_in_remote = launch_shell_command(branch_was_gone_command,
                                         result_as_bool=True)
    delete_branch_is_safe = counterpart_has_full_branch_history(branch)

    if not_in_remote and delete_branch_is_safe:
        return True
    elif not_in_remote and not delete_branch_is_safe:
        counterpart_branch = get_counterpart_branch(branch)
        error_msg = """
        {0}WARNING: La rama {1} no existe en remoto y tiene commits 
        que no existen en {5}{3}.
        
        REVISA si es seguro borrarla y elimínala manualmente con:
            {2}git br -D {1}
        {3}o publícala de nuevo con:
            {4}git flow {1} publish{3}
        """.format(WARNING, branch, INSECURE_COMMAND, RESET_COLOR,
                   SECURE_COMMAND, counterpart_branch)
        exit_with_error_msg(error_msg)
    else:
        return False


def get_current_project_version() -> Version:
    """Devuelve la versión del fichero tag_version del proyecto."""
    with open("tag_version") as file:
        current_tag = file.readline()
    version_digit_list = current_tag.split('.')
    version_digit_list = [int(digit) for digit in version_digit_list]
    return Version(*version_digit_list)


def get_new_release_version() -> Version:
    """Incrementa el segundo dígito y establece el tercer dígito a cero"""
    current_version = get_current_project_version()
    new_minor = 0
    new_version = Version(current_version.major,
                          current_version.release + 1,
                          new_minor)
    return new_version


def get_new_minor_version() -> Version:
    """Incrementa el tercer dígito."""
    current_version = get_current_project_version()
    new_version = Version(current_version.major,
                          current_version.release,
                          current_version.minor + 1)
    print('New version {}'.format(new_version))
    return new_version


def get_current_remote_git_tags() -> List[str]:
    """Devuelve la lista de tags del repositorio git."""
    return launch_shell_command("git ls-remote --tags origin | grep -v { | "
                                "cut -f3 -d '/'", return_list=True)


def update_project_version_if_needed(new_version: Version,
                                     production: bool = False) -> None:
    """Actualiza la versión en el fichero del proyecto tag_version"""

    current_branch_type = get_current_branch_type()

    expected_branch_types = ['release', 'hotfix']

    if current_branch_type not in expected_branch_types:
        error_msg = """
        ERROR: No es posible actualizar la versión del proyecto porque la 
        rama actual no es de tipo release o hotfix.    
        """
        exit_with_error_msg(error_msg, FAIL)

    new_git_tag = new_version.production_tag if production \
        else new_version.release_tag

    current_remote_tags = get_current_remote_git_tags()

    if new_git_tag in current_remote_tags:
        warn_msg = """
        {}WARNING: la versión actual ya existe en el repositorio,  
        no se realizan cambios.{}
        """.format(WARNING, RESET_COLOR)
        print(warn_msg)
    else:
        set_new_version_to_project_files(new_version)
        _create_and_push_new_remote_tag(new_git_tag,
                                        current_branch_type)


def set_new_version_to_project_files(new_version: Version) -> None:
    """Establece una nueva versión en los ficheros del proyecto."""
    current_version = get_current_project_version()

    if current_version == new_version:
        return
    else:
        version_string = new_version.to_string
        with open("tag_version", "w") as file:
            file.write(version_string)

        if os.path.isfile("package.json"):
            launch_shell_command('npm --no-git-tag-version version {}'.format(
                version_string))

        _commit_and_push_changes_in_versioned_files()


def finish_current_branch_and_exit_if_counterpart_has_the_same_code(
        private_branch_finish_command: str,
        close_scrum_task: Callable[[str], None]) -> None:
    """Cierra la rama privada actual y sale si no aporta código nuevo a su
    contraparte cerrando la tarea scrum."""

    expected_branch_type = 'private'

    current_branch = get_current_branch_full_name()
    current_branch_id = get_current_branch_id()

    if counterpart_has_full_branch_history(current_branch):
        try_sequential_git_commands(expected_branch_type,
                                    [private_branch_finish_command])

        close_scrum_task(current_branch_id)

        error_msg = """
        {}WARNING: Se ha finalizado la rama {} sin aportar cambios.{}
        """.format(WARNING, current_branch, RESET_COLOR)
        exit_with_error_msg(error_msg)


def create_new_private_branch(new_branch_type: str,
                              branch_id: int) -> None:
    """Crea una nueva rama privada con un id."""

    if branch_id is None:
        error_msg = """"
            {}Se esperaba un identificador para la nueva rama.{}
            
            No se realizan cambios.
            """.format(FAIL, RESET_COLOR)
        exit_with_error_msg(error_msg)

    expected_branch_type = 'public'

    manage_stash = branch_is_dirty() and is_public_branch()

    if manage_stash:
        launch_shell_command('git add -A')
        launch_shell_command('git stash')
        print("""
        {}WARNING: hay cambios pendientes que se han guardado en el "stash".

        Comprobar que son restaurados más adelante o, en caso de error, hacerlo
        manualmente con el comando:
        
            {}git stash pop{}

        """.format(WARNING, INSECURE_COMMAND, RESET_COLOR))

    public_branch = get_counterpart_branch(new_branch_type)

    public_branch_checkout_command = 'git checkout {}'.format(
        public_branch)
    upd_command = 'git pull --prune'
    start_branch_command = 'git flow {} start {} {}'.format(
        new_branch_type, branch_id, get_current_branch_full_name())
    publish_command = 'git flow publish'

    command_list = [
        public_branch_checkout_command,
        upd_command,
        start_branch_command,
        publish_command]
    success_msg = '{}Rama {} {} creada{}'.format(OK_GREEN,
                                                 new_branch_type,
                                                 branch_id, RESET_COLOR)
    try_sequential_git_commands(expected_branch_type, command_list,
                                success_msg)
    if manage_stash:
        try:
            launch_shell_command('git stash pop')
        except RuntimeError as Error:
            print("""
            {}ERROR: conflicto con los cambios pendientes recuperados del  
            "stash" en la rama privada.
            
            RESOLVERLO MANUALMENTE accediendo a:
            
                Pycharm => VCS => Git => Resolve conflicts...
                
            Una vez resulto, si se ha producido durante un rebase, podemos 
            continuar con normalidad usando el comando:
            
                {}git rebase --continue{}
            
            """.format(FAIL, INSECURE_COMMAND, RESET_COLOR))

            exit_with_error_msg(Error, FAIL)


def get_project_repo_url() -> str:
    """Devuelve el path al repositorio remoto del proyecto"""
    project_repository_partial_url = launch_shell_command(
        'git remote show origin -n | grep "Fetch URL:" | '
        'sed "s/.*://;s/.git$//"')
    return project_repository_partial_url


def create_pull_request_to_master_and_checkout_to_develop(
        source_branch: str) -> None:
    """Crea un pull request a master y hace checkout a develop."""
    pull_request_command = 'chromium-browser --app=' \
                           'https://bitbucket.org/{}/pull-requests/new?' \
                           'source={}\&' \
                           'dest={}\&' \
                           'close_branch=yes'.format(get_project_repo_url(),
                                                     source_branch,
                                                     'master')
    checkout_to_develop_command = 'git checkout develop'
    try_sequential_git_commands(get_current_branch_type(), [
        pull_request_command,
        checkout_to_develop_command
    ])


def merge_current_branch_to_develop(current_branch: str) -> None:
    """Fusiona la rama actual con develop"""

    current_branch_type = get_branch_type(current_branch)
    current_branch_name = get_current_branch_full_name()

    checkout_develop_command = 'git checkout develop'

    merge_current_branch_to_develop_command = 'git merge {}' \
        .format(current_branch_name)

    push_develop_command = 'git push'

    checkout_current_branch_command = 'git co {}'.format(current_branch_name)

    success_msg = '{}Se ha fusionado {} con develop.{}'.format(
        OK_GREEN, current_branch, RESET_COLOR)

    error_msg = """
        {}Error al fusionar la rama {} con develop, resuelve el conflicto 
        manualmente y lanza de nuevo el script anterior.{} 
        """.format(FAIL, current_branch, RESET_COLOR)

    try_sequential_git_commands(current_branch_type, [
        checkout_develop_command,
        merge_current_branch_to_develop_command,
        push_develop_command,
        checkout_current_branch_command
    ], success_msg, error_msg, verbose=True)


def add_files_with_extension(extension: str) -> None:
    """Añade los ficheros con esta extensión al seguimiento de git."""
    if not have_untracked_files():
        exit(0)

    is_public = is_public_branch()
    current_branch_type = get_current_branch_type()

    if is_public and current_branch_type != 'release':
        error_msg = """
        ERROR: La rama publica {} no permite añadir ficheros a git de 
        forma automática.

        revisa si se han creado nuevos ficheros "{}" con el comando:
            {}git sst{}

        y resuelve el estado actual manualmente según sea necesario.
        """.format(current_branch_type.upper(),
                   extension,
                   SECURE_COMMAND,
                   FAIL)
        exit_with_error_msg(error_msg, FAIL)

    have_previous_changes_ready_to_commit = have_changes_ready_to_commit()

    launch_shell_command(
        "git ls-files [path] | grep '\.{}$' | xargs git add".format(extension))

    commit_msg = '"{}" files added by tbm script'.format(extension)

    _create_and_push_optimal_commit_if_applicable(
        have_previous_changes_ready_to_commit,
        commit_msg)


""""
FIN DE API PUBLICA.
"""


def _create_and_push_new_remote_tag(new_git_tag: str,
                                    expected_branch_type: str) -> None:
    tag_comment = 'added by tbm script'
    add_tag_command = 'git tag {} -a -m "{}"'.format(new_git_tag, tag_comment)
    push_tag_to_remote_command = 'git push origin {}'.format(new_git_tag)

    success_msg = '{}Se ha actualizado la versión del proyecto.{}'.format(
        OK_BLUE, RESET_COLOR)

    error_msg = """
        {}ERROR: fallo al subir el nuevo tag a remoto. Es posible que 
        no haya conexión de red, una vez solucionado sincroniza el nuevo tag
        con este comando:
            {}{}{} 
    """.format(FAIL, SECURE_COMMAND, push_tag_to_remote_command,
               RESET_COLOR)

    try_sequential_git_commands(expected_branch_type, [
        add_tag_command,
        push_tag_to_remote_command,
    ], success_msg, error_msg)


def _commit_and_push_changes_in_versioned_files() -> None:
    """Añade los cambios en los ficheros y hace commit adecuado al tipo de
    rama según sea necesario."""

    have_previous_changes_ready_to_commit = have_changes_ready_to_commit()

    posible_versioned_files_to_add = [
        'tag_version',
        'package.json',
        'package-lock.json',
    ]
    for versioned_file in posible_versioned_files_to_add:
        if os.path.isfile(versioned_file):
            launch_shell_command('git add {}'.format(versioned_file))

    commit_msg = "Project version updated by tbm script"
    _create_and_push_optimal_commit_if_applicable(
        have_previous_changes_ready_to_commit,
        commit_msg)


def _create_and_push_optimal_commit_if_applicable(
        have_previous_changes_ready_to_commit: bool,
        commit_msg: str) -> None:
    if have_previous_changes_ready_to_commit or not \
            all_changes_are_ready_to_commit():
        error_msg = """
        WARNING: La rama contiene archivos adicionales pendientes de add y/o
        commit.

        No se ha realizado un nuevo commit y push de forma automática. 
        """
        exit_with_error_msg(error_msg)

    if is_public_branch() or counterpart_has_full_branch_history(
            get_current_branch_full_name()):
        launch_shell_command('git commit -m "{}"'.format(commit_msg))
        launch_shell_command('git push')
    else:
        launch_shell_command('git commit --amend --no-edit')
        launch_shell_command('git push -f')


def _try_sequential_shell_commands(
        command_list: List[str],
        success_msg: str, error_msg: str,
        git_branch_may_be_clean: bool = False,
        verbose: bool = False) -> None:
    try:
        for command in command_list:
            if git_branch_may_be_clean:
                exit_if_pending_changes()
            launch_shell_command(command, verbose=verbose)
    except RuntimeError as error:
        if error_msg is not None:
            exit_with_error_msg(error_msg)
        else:
            exit_with_error_msg("Runtime error: {}".format(error))
    else:
        if success_msg is not None:
            print(success_msg)


def _binary_to_str(message: bytes) -> str:
    return str(message, 'utf-8').replace('\n', '')


def _get_element_from_line(raw_line: bytes) -> str:
    line = _binary_to_str(raw_line)
    return line.replace('*', '').replace(' ', '')


def _print_process_stdout(process_stdout: subprocess.PIPE) -> None:
    if process_stdout is not None:
        for line in process_stdout.readlines():
            line = _binary_to_str(line)
            print(line)
