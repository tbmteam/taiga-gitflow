"""Modulo para las utilidades de taiga_gitflow."""
from getpass import getpass

# TODO: hemos tenido problemas con python-secretstorage porque solo funciona
# la versión compilada por ubuntu del sistema, esto se puede resolver en
# Ubuntu 18.04 creando un entorno virtual con mkvirtualenv y asociandole el
# paquete o paquetes de sistema que den problemas de esta forma:
# add2virtualenv usr/lib/python3/dist-packages/secretstorage
# localizar los paquetes necesarios instalados por apt con:
# locate secretstorage
import keyring
from colorama import Fore, Style, init

OK_BLUE = Fore.BLUE
OK_GREEN = Fore.GREEN
WARNING = Fore.YELLOW
INFO = Fore.MAGENTA
FAIL = Fore.RED
ODD = Fore.WHITE
EVEN = Fore.CYAN
SECURE_COMMAND = Fore.LIGHTBLUE_EX
INSECURE_COMMAND = Fore.LIGHTRED_EX
RESET_COLOR = Style.RESET_ALL

# init color definition
init()


class SessionManager:
    """Gestor de la sesión de Login para una API."""

    def __init__(self, service: str, login: str = None):
        self._login = login if login is not None else self._get_login()
        self._service = service

    @property
    def login(self):
        """Nombre de la cuenta."""
        return self._login

    def request_password(self) -> str:
        """Toma el password del keyring o lo solicita al usuario."""
        password = keyring.get_password(self._service, self._login)

        if password is None:
            password = self._request_password_to_user()
        return password

    def remove_password_from_keyring(self) -> None:
        """Borra una credencial del keyring."""
        keyring.delete_password(self._service, self._login)

    @staticmethod
    def _get_login() -> str:
        print("Username:")
        username = input()
        if username is None:
            raise RuntimeError('el nombre de usuario no es valido')
        else:
            return username

    def _request_password_to_user(self) -> str:
        user_msg = """
            Login to {0}{1}{2} service...
            Account {0}{3}{2}:
            Please, verify your data and enter your password account:
            """.format(INFO, self._service, RESET_COLOR, self._login)

        password = ''
        while password == '' or password is None:
            password = getpass(user_msg)
        keyring.set_password(self._service, self._login, password)
        return password


def exit_with_error_msg(message: [str, Exception],
                        severity: [FAIL, WARNING, OK_BLUE, OK_GREEN
                                   ] = WARNING) -> None:
    """Sale al prompt con mensaje de error y código error level 1."""
    print(severity + message + RESET_COLOR)
    exit(1)
