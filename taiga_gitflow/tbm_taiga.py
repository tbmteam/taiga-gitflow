"""Modulo para el soporte de taiga."""
from collections import OrderedDict
from enum import Enum
from os import environ
from typing import TypeVar, List, Dict
from json import load

from taiga import TaigaAPI
from taiga.exceptions import TaigaRestException

from taiga_gitflow.tbm_git import (
    create_new_private_branch,
)
from taiga_gitflow.tbm_menu import Option, Menu
from taiga_gitflow.tbm_util import (
    WARNING, RESET_COLOR, FAIL,
    SessionManager, exit_with_error_msg)

with open("taiga_config.json") as file:
    taiga_config = load(file)


def do_nothing():
    """Fake callback."""
    pass


Sprint = TypeVar('Sprint')
UserStory = TypeVar('UserStory')
Task = TypeVar('Task')
Issue = TypeVar('Issue')
Project = TypeVar('Project')
User = TypeVar('User')


class TbMEnum(Enum):
    """Enum que devuelve value cuando se convierte a string."""

    def __str__(self):
        return self.value


class TaskStatus(TbMEnum):
    """Estados de una tarea de taiga."""
    NEW = 'New'
    IN_PROGRESS = 'In progress'
    READY_FOR_TEST = 'Ready for test'
    CLOSED = 'Closed'
    NEEDS_INFO = 'Needs info'


class IssueStatus(TbMEnum):
    """Estados de un issue de taiga."""
    NEW = 'New'
    IN_PROGRESS = 'In progress'
    READY_FOR_TEST = 'Ready for test'
    CLOSED = 'Closed'
    NEEDS_INFO = 'Needs info'
    REJECTED = 'Rejected'
    POSTPONED = 'Postponed'


class IssuePriority(TbMEnum):
    """Prioridades de un issue de taiga."""
    LOW = 'Low'
    HIGH = 'High'


class IssueSeverity(TbMEnum):
    """Nivel de severidad de un issue de taiga."""
    WISHLIST = 'Wishlist'
    NORMAL = 'Normal'
    MINOR = 'Minor'
    MAJOR = 'Major'


class IssueType(TbMEnum):
    """Tipos de issue de taiga."""
    ENHANCEMENT = 'Enhancement'
    BUGFIX = 'Bugfix'
    HOTFIX = 'Hotfix'


class TaigaProjectManager:
    """Gestor de proyecto Taiga."""
    _api = TaigaAPI()
    _service = 'taiga'

    def __init__(self,):
        system_user = environ['USER']

        try:
            taiga_login = taiga_config[
                'user_system_to_taiga_login'][system_user]
        except KeyError:
            exit_with_error_msg("""
            ERROR: no se encuentra el login para el usuario "{}" en la 
            configuración de taiga del proyecto actual.
            """.format(system_user), FAIL)
        else:
            self._session = SessionManager(self._service, taiga_login)

        self._auth()
        self._user: User = self._api.me()
        self._project: Project = self._api.projects.get_by_slug(
            taiga_config['project_slug'])
        self._sprint: Sprint = self._get_current_sprint()

    def get_tasks_menu_options(self, tasks: List[Task]) -> List[Option]:
        """Devuelve las tareas como opciones para usar en menú."""
        task_menu_items = []

        for task in tasks:
            description = [task.ref, self.get_task_status_by_taiga_id(
                task.status), task.subject]

            task_menu_items.append(Option(self._description_items_to_columns(
                description), create_new_private_branch, ['feature',
                                                          task.ref],
                self._set_task_status, [task, TaskStatus.IN_PROGRESS]))

        return task_menu_items

    def get_user_hotfixes_menu_options(self) -> List[Option]:
        """Devuelve los hotfix del usuario listos para usar en menú."""
        hotfix_menu_items = []

        for hotfix in self.get_user_hotfixes():
            description = [
                hotfix.ref,
                self._get_priority_taiga_name_by_id(
                    hotfix.priority),
                self.get_issue_status_by_taiga_id(
                    hotfix.status),
                hotfix.subject,
            ]

            hotfix_menu_items.append(
                Option(self._description_items_to_columns(description),
                       create_new_private_branch,
                       ['hotfix', hotfix.ref],
                       self._set_issue_status,
                       [hotfix, IssueStatus.IN_PROGRESS]))

        return hotfix_menu_items

    def get_user_bugfixes_menu_options(self) -> List[Option]:
        """Devuelve los bugfixes del usuario listos para usar en menú."""
        bugfix_menu_items = []

        for bugfix in self.get_user_bugfixes():
            description = [
                bugfix.ref,
                self._get_priority_taiga_name_by_id(
                    bugfix.priority),
                self.get_issue_status_by_taiga_id(
                    bugfix.status),
                bugfix.subject,
            ]

            bugfix_menu_items.append(
                Option(self._description_items_to_columns(description),
                       create_new_private_branch,
                       ['bugfix', bugfix.ref], self._set_issue_status,
                       [bugfix, IssueStatus.IN_PROGRESS]))

        return bugfix_menu_items

    def get_user_enhancements_menu_options(self) -> List[Option]:
        """Devuelve los enhancements del usuario listas para usar en menú."""
        enhancements_menu_items = []

        for enhancement in self.get_user_enhancements():
            description = [
                enhancement.ref,
                self._get_priority_taiga_name_by_id(
                    enhancement.priority),
                self.get_issue_status_by_taiga_id(
                    enhancement.status),
                enhancement.subject,
            ]

            enhancements_menu_items.append(Option(
                self._description_items_to_columns(description),
                create_new_private_branch, ['feature', enhancement.ref],
                self._set_issue_status,
                [enhancement, IssueStatus.IN_PROGRESS]))

        return enhancements_menu_items

    def get_user_stories_menu_options(self) -> List[Option]:
        """Devuelve las historias del usuario listas para usar en menú."""
        stories_menu_items = []

        user_stories_with_tasks = self.get_sprint_user_stories_user_tasks()

        for story in user_stories_with_tasks:
            tasks = user_stories_with_tasks[story]

            description = [story.ref, story.subject]

            task_submenu = Menu(title='Submenú tareas de: "{}"'.format(
                self._truncate_subject(story.subject, 30)))

            task_options = self.get_tasks_menu_options(tasks)

            task_options.append(Option("Volver al Menú de Historias",
                                       task_submenu.close,
                                       [], do_nothing, []))

            task_submenu.set_options(task_options)

            stories_menu_items.append(
                Option(self._description_items_to_columns(description),
                       task_submenu.open, [], do_nothing, []))

        return stories_menu_items

    def get_sprint_user_stories_user_tasks(self
                                           ) -> Dict[UserStory, List[Task]]:
        """Devuelve un diccionario con las historias de usuario y la lista
        de tareas para cada historia."""
        user_stories_user_open_tasks = OrderedDict()

        for user_story in self._sprint.user_stories:
            user_open_tasks = self._get_user_story_user_open_tasks(user_story)
            if user_open_tasks:
                user_stories_user_open_tasks[user_story] = user_open_tasks

        return user_stories_user_open_tasks

    def get_user_hotfixes(self) -> List[Issue]:
        """Devuelve la lista de hotfixes abiertos del usuario."""
        type_id = self._get_id_from_issue_type(IssueType.HOTFIX)

        hotfixes = self._api.issues.list(project=self._project.id,
                                         assigned_to=self._user.id,
                                         type=type_id,
                                         order_by='priority',
                                         status__is_closed=False)
        return hotfixes

    def get_user_bugfixes(self) -> List[Issue]:
        """Devuelve la lista de bugfixes abiertos del usuario."""
        type_id = self._get_id_from_issue_type(IssueType.BUGFIX)
        bugfixes = self._api.issues.list(project=self._project.id,
                                         assigned_to=self._user.id,
                                         type=type_id,
                                         order_by='priority',
                                         status__is_closed=False)
        return bugfixes

    def get_user_enhancements(self) -> List[Issue]:
        """Devuelve la lista de mejoras abiertos del usuario."""
        type_id = self._get_id_from_issue_type(IssueType.ENHANCEMENT)
        enhancements = self._api.issues.list(project=self._project.id,
                                             assigned_to=self._user.id,
                                             type=type_id,
                                             order_by='priority',
                                             status__is_closed=False)
        return enhancements

    def get_task_by_ref(self, task_ref: int) -> Task:
        """Devuelve la tarea a través del ref indicado."""
        return self._project.get_task_by_ref(task_ref)

    def get_issue_by_ref(self, issue_ref: int) -> Issue:
        """Devuelve el issue con el ref indicado."""
        return self._project.get_issue_by_ref(issue_ref)

    def get_issue_status_by_taiga_id(self, issue_status_id: int) -> \
            IssueStatus:
        """Devuelve el estado de una issue por su id de Taiga."""
        for api_issue_status in self._project.issue_statuses:
            if api_issue_status.id == issue_status_id:
                return IssueStatus(api_issue_status.name)

    def get_task_status_by_taiga_id(self, task_status_id: int) -> TaskStatus:
        """Devuelve el estado de la tarea por su id de Taiga."""
        for api_task_status in self._project.task_statuses:
            if api_task_status.id == task_status_id:
                return TaskStatus(api_task_status.name)

    def get_taiga_object_by_ref(self, taiga_ref: int) -> [Task, Issue, None]:
        """Devuelve el objeto de taiga que corresponde con la referencia."""
        try:
            task = self.get_task_by_ref(taiga_ref)
            task.is_task = True
            return task
        except TaigaRestException:
            try:
                issue = self.get_issue_by_ref(taiga_ref)
                issue.is_task = False
                return issue
            except TaigaRestException:
                return None

    def close_taiga_object_by_ref(self, taiga_ref: int):
        """Cierra el objeto que tiene esa referencia."""
        taiga_object = self.get_taiga_object_by_ref(taiga_ref)

        if taiga_object is None:
            print("""{}ERROR: El objeto {} ha sido eliminado de Taiga.
            {}""".format(FAIL, taiga_ref, RESET_COLOR))
        elif taiga_object.is_closed:
            print(
                """{}WARNING: El objeto {} ya estaba cerrado en Taiga.
                {}""".format(WARNING, taiga_ref, RESET_COLOR))
        else:
            if taiga_object.is_task:
                self._set_task_status(taiga_object, TaskStatus.CLOSED)
            else:
                self._set_issue_status(taiga_object, IssueStatus.CLOSED)

    def _set_task_status(self, task: Task, status: TaskStatus) -> None:
        """Cambia el estado de una tarea."""
        task.patch(['version'], status=self._get_id_from_task_status(status))

    def _set_issue_status(self, issue: Issue, status: IssueStatus) -> None:
        """Cambia el estado de una issue."""
        issue.patch(['version'], status=self._get_id_from_issue_status(status))

    @staticmethod
    def _truncate_subject(subject: str, length: int = 150):
        return (subject[:length] + '...') if len(subject) > length else subject

    def _get_user_story_user_open_tasks(self, user_story: UserStory
                                        ) -> List[Task]:
        """Devuelve las tareas abiertas de una historia de usuario."""
        return user_story.list_tasks().filter(is_closed=False,
                                              assigned_to=self._user.id)

    def _get_current_sprint(self) -> Sprint:
        """Devuelve el sprint actual."""
        try:
            sprint = self._api.milestones.list(project=self._project.id,
                                               closed=False)[0]
        except IndexError:
            exit_with_error_msg("""
                       ERROR: no hay un sprint abierto en este momento.
                       """, FAIL)
        else:
            return sprint

    def _description_items_to_columns(self,
                                      description_items: List[str]) -> str:

        id_width = "{:<8}"
        detail_width = " {:<15}"
        subject_width = "{:<155}"

        columns_format = id_width

        for item_index in range(2, len(description_items)):
            columns_format += detail_width

        columns_format += subject_width

        description_items[0] = 'TG-' + str(description_items[0])
        description_items[-1] = self._truncate_subject(description_items[-1])

        return columns_format.format(*description_items)

    def _get_priority_taiga_name_by_id(self, priority_id: int) -> str:
        for api_priority in self._project.priorities:
            if api_priority.id == priority_id:
                return api_priority.name

    def _get_id_from_task_status(self, task_status: TaskStatus) -> int:
        for api_task_status in self._project.task_statuses:
            if api_task_status.name == task_status.value:
                return api_task_status.id

    def _get_id_from_issue_status(self, issue_status: IssueStatus) -> int:
        for api_issue_status in self._project.issue_statuses:
            if api_issue_status.name == issue_status.value:
                return api_issue_status.id

    def _get_id_from_issue_type(self, issue_type: IssueType) -> int:
        for api_issue_type in self._project.issue_types:
            if api_issue_type.name == issue_type.value:
                return api_issue_type.id

    def _auth(self) -> None:
        try:
            self._api.auth(
                self._session.login,
                self._session.request_password()
            )
        except TaigaRestException:
            self._session.remove_password_from_keyring()
            exit_with_error_msg("""
            ERROR: error al iniciar sesión, vuelve a introducir la 
            contraseña.
            """, FAIL)


def close_taiga_task(task_ref: [str, int]) -> None:
    """Cierra la tarea del proyecto Taiga actual."""
    task_ref = int(task_ref)
    taiga_project = TaigaProjectManager()
    taiga_project.close_taiga_object_by_ref(task_ref)
