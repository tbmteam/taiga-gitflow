"""Modulo para el soporte de menús de entrada."""
import os
from time import sleep
from typing import List, Callable, Any, NamedTuple
from taiga_gitflow.tbm_util import ODD, EVEN, RESET_COLOR


def do_nothing():
    """Fake callback."""
    pass


def close_menu():
    """Fake handler para detectar el cierre del menú."""
    pass


class Option(NamedTuple):
    """Clase de datos para las opciones de los menús."""
    description: str
    handler: Callable
    args: List[Any]
    callback: Callable
    callback_args: List[Any]

    def execute(self):
        """Ejecuta la opción."""
        self.handler(*self.args)
        self.callback(*self.callback_args)


class Menu(object):
    """Gestiona un menú de entrada de usuario."""

    def __init__(self,
                 options: List[Option] = None,
                 title: str = None, message: str = None,
                 prompt: str = ">>>", refresh: Callable = lambda: None):
        self.options: List[Option] = options if options is not None else []
        self.title: str = title
        self.is_title_enabled: bool = True
        self.message: str = message
        self.is_message_enabled: bool = True if message is not None else False
        self.refresh = refresh
        self.prompt: str = prompt
        self.is_open: bool = None

    def set_options(self, options: List[Option]) -> None:
        """Comprueba el formato de las opciones y las pasa al método que lo
        añade al atributo de opciones."""
        self.options = options

    def set_title(self, title: str) -> None:
        """Establece el título."""
        self.title = title

    def set_title_enabled(self, is_enabled: bool) -> None:
        """Habilita el título"""
        self.is_title_enabled = is_enabled

    def set_message(self, message: str) -> None:
        """Establece el mensaje"""
        self.message = message

    def set_message_enabled(self, is_enabled: bool) -> None:
        """Habilita el mensaje"""
        self.is_message_enabled = is_enabled

    def set_prompt(self, prompt: str) -> None:
        """Establece el prompt que se muestra en consola."""
        self.prompt = prompt

    def set_refresh(self, refresh: Callable) -> None:
        """Dibuja lo que se muestra en consola."""
        self.refresh = refresh

    def add_option(self, option: Option) -> None:
        """Añade una opción a la lista de opciones"""
        self.options.append(option)

    def open(self) -> None:
        """Abre el menú"""
        self.is_open = True
        while self.is_open:
            self.refresh()
            option: Option = self.input()
            option.execute()

            if option.callback.__name__ != do_nothing.__name__:
                exit(0)

            print()
            sleep(0.01)

    def close(self) -> None:
        """Cierra el menú"""
        self.is_open = False

    def show(self) -> None:
        """Limpia la consola y muestra las opciones del menú"""
        os.system('cls' if os.name == 'nt' else 'clear')
        if self.is_title_enabled:
            print(self.title)
            print()
        if self.is_message_enabled:
            print(self.message)
            print()
        for index, option in enumerate(self.options):
            is_odd_line = (index & 1) == 1

            if is_odd_line:
                option_color = ODD
            else:
                option_color = EVEN
            print("{}{}. {}{}".format(option_color,
                                      str(index + 1),
                                      option.description,
                                      RESET_COLOR))
        print()

    def input(self) -> Option:
        """Muestra el menú, recoge la opción seleccionada por el usuario y
        devuelve el handler y la referencia correspondiente a la tarea
        seleccionada."""
        if len(self.options) == 0:
            close_menu_option = Option('No options', Menu.close, [],
                                       do_nothing, [])
            return close_menu_option
        try:
            self.show()
            index = int(input(self.prompt + " ")) - 1
            return self.options[index]
        except (ValueError, IndexError):
            return self.input()
