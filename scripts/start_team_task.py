#! /usr/bin/env python
"""Script que inicia una nueva rama bugfix."""
from taiga_gitflow.tbm_menu import Menu, Option, do_nothing
from taiga_gitflow.tbm_taiga import TaigaProjectManager


def main():
    """Ejecuta el script."""
    project = TaigaProjectManager()

    user_stories_submenu_options = project.get_user_stories_menu_options()
    enhancements_submenu_options = project.get_user_enhancements_menu_options()
    bugfixes_submenu_options = project.get_user_bugfixes_menu_options()
    hotfixes_submenu_options = project.get_user_hotfixes_menu_options()

    main_menu = Menu(title="Menú Principal. Tareas de Taiga")

    main_menu_options = []

    if user_stories_submenu_options:
        user_stories_submenu: Menu = Menu(title="Menú Historias de Usuario ")

        main_menu_options.append(
            Option("Historias de Usuario", user_stories_submenu.open, [],
                   do_nothing, []))

        user_stories_submenu_options.append(Option("Volver al Menú Principal",
                                                   user_stories_submenu.close,
                                                   [], do_nothing, []))
        user_stories_submenu.set_options(user_stories_submenu_options)

    if enhancements_submenu_options:
        enhancements_submenu: Menu = Menu(title="Menú Enhancements")

        main_menu_options.append(
            Option("Enhancements", enhancements_submenu.open, [], do_nothing,
                   []))

        enhancements_submenu_options.append(Option("Volver al Menú Principal",
                                                   enhancements_submenu.close,
                                                   [], do_nothing, []))
        enhancements_submenu.set_options(enhancements_submenu_options)

    if bugfixes_submenu_options:
        bugfixes_submenu: Menu = Menu(title="Menú Bugfixes")

        main_menu_options.append(
            Option("Bugfixes", bugfixes_submenu.open, [], do_nothing, []))

        bugfixes_submenu_options.append(Option("Volver al Menú Principal",
                                               bugfixes_submenu.close, [],
                                               do_nothing, []))
        bugfixes_submenu.set_options(bugfixes_submenu_options)

    if hotfixes_submenu_options:
        hotfixes_submenu = Menu(title="Menú Hotfixes")

        main_menu_options.append(
            Option("Hotfixes", hotfixes_submenu.open, [], do_nothing, []))

        hotfixes_submenu_options.append(Option("Volver al Menú Principal",
                                               hotfixes_submenu.close, [],
                                               do_nothing, []))
        hotfixes_submenu.set_options(hotfixes_submenu_options)

    main_menu_options.append(
        Option("Salir", main_menu.close, [], do_nothing, []))
    main_menu.set_options(main_menu_options)

    main_menu.open()


if __name__ == "__main__":
    main()
