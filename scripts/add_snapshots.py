#! /usr/bin/env python
"""Script que añade los ficheros snap de los tests a git."""
from taiga_gitflow.tbm_git import add_files_with_extension


def main():
    """Ejecuta el script."""
    add_files_with_extension('snap')


if __name__ == "__main__":
    main()
