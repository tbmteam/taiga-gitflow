#! /usr/bin/env python
"""Script que actualiza solo los ficheros de versión de una rama feature.
."""
from taiga_gitflow.tbm_git import (
    set_new_version_to_project_files,
    get_new_minor_version,
    get_current_branch_type,
    exit_with_error_msg, FAIL
)


def main():
    """Ejecuta el script."""

    expected_branch_type = 'feature'
    current_branch_type = get_current_branch_type()

    if expected_branch_type != current_branch_type:
        exit_with_error_msg(
            "ERROR: La rama actual no es del tipo esperado {}".format(
                expected_branch_type.upper()),
            FAIL
        )

    set_new_version_to_project_files(get_new_minor_version())


if __name__ == "__main__":
    main()
