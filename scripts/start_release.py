#! /usr/bin/env python
"""Script que inicia una nueva rama release."""
from taiga_gitflow.tbm_git import (try_sequential_git_commands,
                                 get_new_release_version,
                                 update_project_version_if_needed)
from taiga_gitflow.tbm_util import OK_BLUE, RESET_COLOR


def main():
    """Ejecuta el script."""

    expected_branch_type = 'develop'

    new_release_version = get_new_release_version()

    upd_command = 'git pull --prune'

    release_start_command = 'git flow release start {}'.format(
        new_release_version.to_string)
    publish_command = 'git flow release publish'

    command_list = [upd_command, release_start_command, publish_command]

    success_msg = "{}Rama Release {} creada.{}".format(
        OK_BLUE,
        new_release_version.to_string,
        RESET_COLOR)

    try_sequential_git_commands(expected_branch_type, command_list,
                                success_msg)

    update_project_version_if_needed(new_release_version)


if __name__ == "__main__":
    main()
