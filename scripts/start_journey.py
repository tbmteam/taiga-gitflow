#! /usr/bin/env python
"""Script para realizar la ceremonia de inicio de jornada para la rama
privada actual."""
from taiga_gitflow.tbm_git import (
    get_current_branch_full_name,
    try_sequential_git_commands,
    update_branch_and_counterpart_and_delete_orphan_branches,
    branch_has_full_counterpart_history,
    get_counterpart_branch,
    is_public_branch,
    launch_shell_command
)
from taiga_gitflow.tbm_util import (
    OK_GREEN, RESET_COLOR, INSECURE_COMMAND, WARNING, SECURE_COMMAND
)


def main():
    """Ejecuta el script."""

    success_msg = '{}Inicio de jornada realizado.{}'.format(OK_GREEN,
                                                            RESET_COLOR)
    if is_public_branch():
        launch_shell_command('git pull --prune')
        print(success_msg)
        exit(0)

    expected_branch_type = 'private'

    current_branch = get_current_branch_full_name()

    counterpart_public_branch = get_counterpart_branch(current_branch)

    update_branch_and_counterpart_and_delete_orphan_branches(current_branch)

    error_msg = """
    Conflicto en rebase.
    
    Resuelve manualmente y continua con el trabajo usando los comandos:
    
        {}git rebase --continue
        git push -f
        
    {}o aborta el rebase con el comando:
        {}git rebase --abort
        
    """.format(INSECURE_COMMAND, WARNING, SECURE_COMMAND)

    if not branch_has_full_counterpart_history(current_branch):
        rebase_command = 'git rebase {}'.format(counterpart_public_branch)
        # desactivado rebase interactivo por poco utilidad:
        # rebase_command = 'git rebase -i {}'.format(counterpart_public_branch)
        push_forced_private_command = 'git push -f'
        git_command_list = [rebase_command, push_forced_private_command]

        try_sequential_git_commands(expected_branch_type, git_command_list,
                                    success_msg, error_msg, verbose=True)
    else:
        print(success_msg)


if __name__ == "__main__":
    main()
