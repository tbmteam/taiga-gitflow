#! /usr/bin/env python
"""Script que finaliza una rama hotfix y la fusiona con master y develop."""
from taiga_gitflow.tbm_git import (
    update_project_version_if_needed,
    get_new_minor_version, try_sequential_git_commands,
    finish_current_branch_and_exit_if_counterpart_has_the_same_code,
    merge_current_branch_to_develop,
    get_current_branch_full_name,
    create_pull_request_to_master_and_checkout_to_develop,
    get_current_branch_id,
)
from taiga_gitflow.tbm_util import OK_BLUE, RESET_COLOR

from taiga_gitflow.tbm_taiga import close_taiga_task


def main():
    """Ejecuta el script."""

    expected_branch_type = 'hotfix'

    current_branch_id = get_current_branch_id()

    start_journey_command = 'tbm_start_journey'
    try_sequential_git_commands(expected_branch_type, [start_journey_command])
    current_branch = get_current_branch_full_name()

    finish_hotfix_command = 'git push origin --delete {0} && git checkout ' \
                            'develop && git branch -D {0}' \
        .format(current_branch)

    finish_current_branch_and_exit_if_counterpart_has_the_same_code(
        finish_hotfix_command, close_taiga_task)

    run_all_test = 'sh ./run_all_tests.sh'

    success_msg = '{}Se ha finalizado la rama hotfix.{}'.format(
        OK_BLUE, RESET_COLOR)

    try_sequential_git_commands(expected_branch_type, [
        run_all_test
    ], success_msg)

    update_project_version_if_needed(get_new_minor_version(),
                                     production=True)

    merge_current_branch_to_develop(expected_branch_type)

    create_pull_request_to_master_and_checkout_to_develop(current_branch)

    close_taiga_task(current_branch_id)


if __name__ == "__main__":
    main()
