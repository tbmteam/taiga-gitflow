#! /usr/bin/env python
"""Script que finaliza una rama bugfix y la fusiona con su release
correspondiente.
."""
from taiga_gitflow.tbm_git import (
    update_project_version_if_needed,
    get_new_minor_version, try_sequential_git_commands,
    merge_current_branch_to_develop,
    finish_current_branch_and_exit_if_counterpart_has_the_same_code,
    get_current_branch_id,
)
from taiga_gitflow.tbm_util import OK_BLUE, RESET_COLOR
from taiga_gitflow.tbm_taiga import close_taiga_task


def main():
    """Ejecuta el script."""

    expected_branch_type = 'bugfix'

    current_branch_id = get_current_branch_id()

    start_journey_command = 'tbm_start_journey'
    try_sequential_git_commands(expected_branch_type, [start_journey_command])

    bugfix_finish_command = 'git flow bugfix finish'

    finish_current_branch_and_exit_if_counterpart_has_the_same_code(
        bugfix_finish_command, close_taiga_task)

    run_all_test = 'sh ./run_all_tests.sh'

    command_list = [run_all_test, bugfix_finish_command]

    success_msg = '{}Se ha finalizado la rama bugfix.{}'.format(
        OK_BLUE, RESET_COLOR)
    try_sequential_git_commands(expected_branch_type, command_list,
                                success_msg)

    expected_branch_type = 'release'

    update_project_version_if_needed(get_new_minor_version())

    merge_current_branch_to_develop(expected_branch_type)

    close_taiga_task(current_branch_id)


if __name__ == "__main__":
    main()
