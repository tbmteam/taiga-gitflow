#! /usr/bin/env python
"""Script que finaliza una rama release fusionandola con develop y master a
través de un PR.
."""
from taiga_gitflow.tbm_git import (
    try_sequential_git_commands,
    get_current_project_version,
    update_project_version_if_needed,
    get_current_branch_full_name,
    create_pull_request_to_master_and_checkout_to_develop,
    merge_current_branch_to_develop,
    update_branch_and_counterpart_and_delete_orphan_branches)


def main():
    """Ejecuta el script."""

    expected_branch_type = 'release'

    current_branch = get_current_branch_full_name()

    update_branch_and_counterpart_and_delete_orphan_branches(current_branch)

    run_all_test = 'sh ./run_all_tests.sh'

    try_sequential_git_commands(expected_branch_type, [run_all_test])

    update_project_version_if_needed(get_current_project_version(),
                                     production=True)

    merge_current_branch_to_develop(current_branch)

    create_pull_request_to_master_and_checkout_to_develop(current_branch)


if __name__ == "__main__":
    main()
