#! /usr/bin/env python
"""Script que finaliza la rama feature actual."""
from taiga_gitflow.tbm_git import (
    try_sequential_git_commands,
    finish_current_branch_and_exit_if_counterpart_has_the_same_code,
    get_current_branch_id
)
from taiga_gitflow.tbm_taiga import close_taiga_task
from taiga_gitflow.tbm_util import OK_BLUE, RESET_COLOR


def main():
    """Ejecuta el script."""

    expected_branch_type = 'feature'

    current_branch_id = get_current_branch_id()

    start_journey_command = 'tbm_start_journey'
    try_sequential_git_commands(expected_branch_type, [start_journey_command])

    finish_feature_command = 'git flow feature finish'

    finish_current_branch_and_exit_if_counterpart_has_the_same_code(
        finish_feature_command, close_taiga_task)

    run_all_tests = 'sh ./run_all_tests.sh'

    develop_push_command = 'git push'

    command_list = [run_all_tests, finish_feature_command,
                    develop_push_command]
    success_msg = 'Finish feature realizado correctamente. Estas en la rama ' \
                  '{}DEVELOP.{}'.format(OK_BLUE, RESET_COLOR)

    try_sequential_git_commands(expected_branch_type, command_list,
                                success_msg)

    close_taiga_task(current_branch_id)


if __name__ == "__main__":
    main()
