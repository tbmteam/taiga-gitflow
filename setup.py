from setuptools import setup, find_packages
from codecs import open
from os import path

with open("tag_version") as file:
    __version__ = file.readline()

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# get the dependencies and installs
with open(path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    all_reqs = f.read().split('\n')

install_requires = [x.strip() for x in all_reqs if 'git+' not in x]
dependency_links = [x.strip().replace('git+', '') for x in all_reqs if x.startswith('git+')]

setup(
    name='taiga_gitflow',
    version=__version__,
    description='Console commands to integrate the current Taiga Scrum '
                'sprint with Trading by Machine team gitflow philosophy.',
    long_description=long_description,
    url='https://bitbucket.org/tbmteam/taiga-gitflow',
    download_url='https://bitbucket.org/tbmteam/taiga-gitflow/tarball/' + __version__,
    license='Copyright, Trading On Machine, S.L, All Rights Reserved.',
    classifiers=[
      'Development Status :: 3 - Alpha',
      'Intended Audience :: Developers',
      'Programming Language :: Python :: 3',
    ],
    keywords='',
    entry_points={
        'console_scripts': [
            'tbm_start_journey = scripts.start_journey:main',
            'tbm_finish_feature = scripts.finish_feature:main',
            'tbm_start_release = scripts.start_release:main',
            'tbm_finish_release = scripts.finish_release:main',
            'tbm_finish_bugfix = scripts.finish_bugfix:main',
            'tbm_finish_hotfix = scripts.finish_hotfix:main',
            'tbm_start_team_task = scripts.start_team_task:main',
            'tbm_new_minor_version = scripts.new_minor_version:main',
            'tbm_add_snapshots = scripts.add_snapshots:main',
        ]
    },
    packages=find_packages(exclude=['docs', 'tests*', 'user_home_skel',
                                    'shell_scripts', 'assets']),
    include_package_data=True,
    author='Trading by Machine',
    install_requires=install_requires,
    dependency_links=dependency_links,
    extras_require={
        'dev': [
            'sphinx',
            'sphinx_rtd_theme',
            'nose',
            'coverage',
            'pypi-publisher',
            'keyring',
        ]
    },
    author_email='info@tradingbymachine.com'
)
